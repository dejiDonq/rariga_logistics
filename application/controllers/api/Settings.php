<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/MY_API_Controller.php');

class Settings extends MY_API_Controller
{
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('settings_model', 'settings');
    }

    public function create_post(){ 

        $output = array();

        try{
            $output = $this->{$this->controller}->create(array(
                'setting_key' => $this->input->post('setting_key'),
                'value' => $this->input->post('value'),
                'status' => NULL !== $this->input->post('status') ? $this->input->post('status') : NULL,
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not add setting to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function index_post($id = NULL){

        $output = array();

        if(isset($id)){

            $filter = (is_numeric($id)) ? array(
                'setting_id' => $id
            ) : array(
                'setting_key' => $id
            );

            try{
                $output = $this->{$this->controller}->read_one($filter);
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve setting from store: '.$e->getMessage());
            }
    
        }
        else{
            $filter = (NULL !== $this->input->post('filter')) ? $this->input->post('filter') : NULL;
            $page = (NULL !== $this->input->post('page')) ? $this->input->post('page') : 1;

            try{
                $output = $this->{$this->controller}->paginate((array) json_decode($filter), $page, 12, 'setting_id');
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve settings from store: '.$e->getMessage());
            }

        }

        return $this->success($output);
    }

    public function update_post($id){
        $output = array();

        try{

            $filter = (is_numeric($id)) ? array(
                'setting_id' => $id
            ) : array(
                'setting_key' => $id
            );

            $input = array_filter($this->input->post(), function($v, $k){
                return in_array($k, $this->{$this->controller}->db_columns());
            }, ARRAY_FILTER_USE_BOTH);
            $output = $this->{$this->controller}->update($filter, $input);
        }
        catch(Exception $e){
            return $this->fail('Could not update setting in store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function delete_post($id){
        $output = array();

        try{

            $filter = (is_numeric($id)) ? array(
                'setting_id' => $id
            ) : array(
                'setting_key' => $id
            );

            $output = $this->{$this->controller}->soft_delete($filter);
        }
        catch(Exception $e){
            return $this->fail('Could not delete setting from store: '.$e->getMessage());
        }

        return $this->success($output);
    }

}