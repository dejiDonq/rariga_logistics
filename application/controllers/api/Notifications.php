<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/MY_API_Controller.php');

class Notifications extends MY_API_Controller
{
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('notifications_model', 'notifications');
    }

    public function create_post(){ 

        $output = array();

        try{
            $output = $this->{$this->controller}->create(array(
                'cust_id' => $this->input->post('cust_id'),
                'message' => $this->input->post('message'),
                'severity' => $this->input->post('severity'),
                'type' => $this->input->post('type'),
                'closed' => NULL !== $this->input->post('closed') ? $this->input->post('closed') : NULL,
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not add notification to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function index_post($id = NULL){

        $output = array();

        if(isset($id)){

            try{
                $output = $this->{$this->controller}->read_one(array(
                    'id' => $id
                ));
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve notification from store: '.$e->getMessage());
            }
    
        }
        else{
            
            $filter = (NULL !== $this->input->post('filter')) ? $this->input->post('filter') : NULL;

            $page = (NULL !== $this->input->post('page')) ? $this->input->post('page') : 1;

            try{
                $output = $this->{$this->controller}->paginate((array) json_decode($filter), $page, 12, 'id');
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve notifications from store: '.$e->getMessage());
            }

        }

        return $this->success($output);
    }

    public function update_post($id){
        $output = array();

        try{
            $input = array_filter($this->input->post(), function($v, $k){
                return in_array($k, $this->{$this->controller}->db_columns());
            }, ARRAY_FILTER_USE_BOTH);
            $output = $this->{$this->controller}->update(array(
                'id' => $id
            ), $input);
        }
        catch(Exception $e){
            return $this->fail('Could not update notification in store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function delete_post($id){
        $output = array();

        try{
            $output = $this->{$this->controller}->soft_delete(array(
                'id' => $id
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not delete notification to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function customers_post($id){

        $output = array();

        try{
            $notification = $this->{$this->controller}->read_one(array(
                'id' => $id
            ));

            if(isset($notification)){
                $output = $this->{$this->controller}->table('customer', 'cust_id')->read_many(array(
                    'cust_id' => $notification['cust_id']
                ));
            }
            else{
                throw new Exception("Notification with given ID not found.");
            }

        }
        catch(Exception $e){
            return $this->fail('Could not retrieve notification\'s customer to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    protected function send_mail($to, $title, $message)
    {
        $this->load->library('email');

        $config = array(
            'protocol' => 'sendmail',
            'mailpath' => '/usr/sbin/sendmail',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
        );

        $this->email->initialize($config);
        
        $this->email->from('no-reply@rar-logistics.com', 'RAR Logistics');
        $this->email->to($to);

        $this->email->subject($title);

        $this->email->message($message);

        $this->email->send();
    }

}