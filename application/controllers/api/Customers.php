<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/MY_API_Controller.php');

class Customers extends MY_API_Controller
{
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('customers_model', 'customers');

    }

    public function create_post(){ 

        $output = array();

        try{
            
            if($this->{$this->controller}->count(array(
                'email' => $this->input->post('email')
            )) != 0){
                return $this->fail('The email address you provided already exists.');
            }

            $output = $this->{$this->controller}->create(array(
                'fname' => $this->input->post('fname'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'pword' => password_hash($this->input->post('pword'), PASSWORD_BCRYPT),
                'status' => NULL !== $this->input->post('status') ? $this->input->post('status') : NULL,
            ));

            if($output){
                $message = "
                Hello ".$this->input->post('fname').",<br />
                Welcome to Rariga Logistics. <br />
                Your account has been activated<br />
                Welcome Aboard.<br />
            ";

            $is_sent = $this->send_mail($this->input->post('email'), 'Welcome Aboard', $message);

            $not = $this->{$this->controller}->table('notifications', 'id')->create(array(
                'cust_id' => $output['cust_id'],
                'message' => "Welcome to Rariga Logistics.",
                'severity' => "medium",
                'type' => "primary",
                'closed' => NULL,
            ));

            }

        }
        catch(Exception $e){
            return $this->fail('Could not add customer to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function index_post($id = NULL){

        $output = array();

        if(isset($id)){

            try{
                $output = $this->{$this->controller}->read_one(array(
                    'cust_id' => $id
                ));
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve customer from store: '.$e->getMessage());
            }
    
        }
        else{

            $filter = (NULL !== $this->input->post('filter')) ? $this->input->post('filter') : NULL;
            $page = (NULL !== $this->input->post('page')) ? $this->input->post('page') : 1;

            try{
                $output = $this->{$this->controller}->paginate((array) json_decode($filter), $page, 12, 'cust_id');
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve customers from store: '.$e->getMessage());
            }

        }

        return $this->success($output);
    }

    public function update_post($id){
        $output = array();

        try{
            $input = array_filter($this->input->post(), function($v, $k){
                return in_array($k, $this->{$this->controller}->db_columns());
            }, ARRAY_FILTER_USE_BOTH);

            $output = $this->{$this->controller}->update(array(
                'cust_id' => $id
            ), array_merge(
                $input,
                (NULL !== $this->input->post('pword')) ? array(
                    'pword' => password_hash($this->input->post('pword'), PASSWORD_BCRYPT)
                ) : array()
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not update customer in store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function delete_post($id){
        $output = array();

        try{
            $output = $this->{$this->controller}->soft_delete(array(
                'cust_id' => $id
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not delete customer to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function login_post(){
        
        $output = array();

        try{
            $customer = $output = $this->{$this->controller}->read_one(array(
                'email' => $this->input->post('email')
            ));

            if(isset($customer)){
                if(!password_verify($this->input->post('password'), $customer['pword'])){
                    return $this->fail('The password you provided does not match our record');
                }
            }
            else{
                return $this->fail('The email you provided doesn\'t exist');
            }
        }
        catch(Exception $e){
            return $this->fail('Login Issues: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function logout_post(){

        $output = array();

        return $this->success($output);
    }


    public function orders_post($id){

        $output = array();

        try{
            $output = $this->{$this->controller}->table('orders', 'ord_id')->read_many(array(
                'cust_id' => $id
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not retrieve customer orders from store: '.$e->getMessage());
        }

        return $this->success($output);
    }
    
    public function notifications_post($id){

        $output = array();

        try{
            $output = $this->{$this->controller}->table('notifications', 'id')->read_many(array(
                'cust_id' => $id
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not retrieve customer notifications from store: '.$e->getMessage());
        }

        return $this->success($output);
    }


    public function reset_password_post($id = null){

        $output = array();

        try{

            $pword = random_string('alnum', 7);



            //change password
            $customer = $this->{$this->controller}->update(array(
                // 'email' => $id
                'email' => $this->input->post('email')
            ), array(
                'pword' => password_hash($pword, PASSWORD_BCRYPT)
            ));

            //send mail to customer
            $message = "
                Hello ".$customer['fname'].",<br />
                A password reset was requested using your account. <br />
                Use this password to login: ".$pword."<br />
                Kindly change the password once you login for the first time.<br />
            ";

            $is_sent = $this->send_mail($customer['email'], 'Password Reset', $message);

            $output = array(
                'new_password' => $pword,
                'mail_sent' => $is_sent,
            );

        }
        catch(Exception $e){
            return $this->fail('There was some issues encountered while trying to reset password: '.$e->getMessage());
        }

        return $this->success($output);
    }


    protected function send_mail($to, $title, $message)
    {
        $this->load->library('email');

        $config = array(
            'protocol' => 'sendmail',
            'mailpath' => '/usr/sbin/sendmail',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
        );

        $this->email->initialize($config);
        
        $this->email->from('no-reply@rariga-logistics.com', 'RARIGA Logistics');
        $this->email->to($to);

        $this->email->subject($title);

        $this->email->message($message);

        $this->email->send();
    }

}