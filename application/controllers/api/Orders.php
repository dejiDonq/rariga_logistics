<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/MY_API_Controller.php');

class Orders extends MY_API_Controller
{
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('orders_model', 'orders');
    }

    public function create_post(){ 

        $output = array();

        try{
            $output = $this->{$this->controller}->create(array(
                'cust_id' => $this->input->post('cust_id'),
                'source' => $this->input->post('source'),
                'dest' => $this->input->post('dest'),
                'cat_id' => $this->input->post('cat_id'),
                'descr' => $this->input->post('descr'),
                'price' => $this->input->post('price'),
                'name' => $this->input->post('name'),
                'recnum' => $this->input->post('recnum'),
                'payOpt' => $this->input->post('payOpt'),
                'booking_date' => $this->input->post('booking_date'),
                'delivery_date' => $this->input->post('delivery_date'),
                'status' => NULL !== $this->input->post('status') ? $this->input->post('status') : NULL,
            ));

            $id = $this->input->post('cust_id');

            $user = $this->{$this->controller}->table('customer', 'cust_id')->read_one(array(
                'cust_id' => $id
            ));

            if($output && $user){
                $message = "
                Hello ".$user['fname'].",<br />
                Your order <b>".$this->input->post('name')."</b> has been received. <br />

                <h4>Details</h4><br/>
                Description: <b>".$this->input->post('descr')."</b> <br />
                Pickup Location: <b>".explode('|||',$this->input->post('source'))[0]."</b> <br />
                Destination: <b>".explode('|||',$this->input->post('dest'))[0]."</b> <br />
                Booked: <b>".$this->input->post('booking_date')."</b> <br />
                Preferred Delivery Date: <b>".$this->input->post('delivery_date')."</b> <br />


                Delivery details will be sent shortly <br />
                Thank you.<br />
            ";

            $is_sent = $this->send_mail($user['email'], 'Order Received', $message);


            $not = $this->{$this->controller}->table('notifications', 'id')->create(array(
                'cust_id' => $id,
                'message' => "Your order <b>".$this->input->post('name')."</b> has been received.",
                'severity' => "medium",
                'type' => "danger",
                'closed' => NULL,
            ));

            }


        }
        catch(Exception $e){
            return $this->fail('Could not add order to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function index_post($id = NULL){

        $output = array();

        if(isset($id)){

            try{
                $output = $this->{$this->controller}->read_one(array(
                    'ord_id' => $id
                ));
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve order from store: '.$e->getMessage());
            }
    
        }
        else{
            
            $filter = (NULL !== $this->input->post('filter')) ? $this->input->post('filter') : NULL;

            $page = (NULL !== $this->input->post('page')) ? $this->input->post('page') : 1;

            try{
                $output = $this->{$this->controller}->paginate((array) json_decode($filter), $page, 12, 'ord_id');
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve orders from store: '.$e->getMessage());
            }

        }

        return $this->success($output);
    }

    public function update_post($id){
        $output = array();

        try{
            $input = array_filter($this->input->post(), function($v, $k){
                return in_array($k, $this->{$this->controller}->db_columns());
            }, ARRAY_FILTER_USE_BOTH);
            $output = $this->{$this->controller}->update(array(
                'ord_id' => $id
            ), $input);
        }
        catch(Exception $e){
            return $this->fail('Could not update order in store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function delete_post($id){
        $output = array();

        try{
            $output = $this->{$this->controller}->soft_delete(array(
                'ord_id' => $id
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not delete order to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function user_post($id){

        $output = array();

        try{
            $order = $this->{$this->controller}->read_one(array(
                'ord_id' => $id
            ));

            if(isset($order)){
                $output = $this->{$this->controller}->table('customer', 'cust_id')->read_one(array(
                    'cust_id' => $order['cust_id']
                ));
            }
            else{
                throw new Exception("Order with given ID not found.");
            }

        }
        catch(Exception $e){
            return $this->fail('Could not retrieve order\'s customer to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function category_post($id){

        $output = array();

        try{
            $order = $this->{$this->controller}->read_one(array(
                'ord_id' => $id
            ));

            if(isset($order)){
                $output = $this->{$this->controller}->table('categories', 'cat_id')->read_one(array(
                    'cat_id' => $order['cat_id']
                ));
            }
            else{
                throw new Exception("Order with given ID not found.");
            }

        }
        catch(Exception $e){
            return $this->fail('Could not retrieve order\'s customer to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    protected function send_mail($to, $title, $message)
    {
        $this->load->library('email');

        $config = array(
            'protocol' => 'sendmail',
            'mailpath' => '/usr/sbin/sendmail',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'newline' => "\r\n",
            'wordwrap' => TRUE,
        );

        $this->email->initialize($config);
        
        $this->email->from('no-reply@rariga-logistics.com', 'RARIGA Logistics');
        $this->email->to($to);

        $this->email->subject($title);

        $this->email->message($message);

        $this->email->send();
    }
    
}