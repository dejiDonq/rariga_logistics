location<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'core/MY_API_Controller.php');

class Locations extends MY_API_Controller
{
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('locations_model', 'locations');

    }

    public function create_post(){ 

        $output = array();

        try{
            $output = $this->{$this->controller}->create(array(
                'name' => $this->input->post('name'),
                'price' => $this->input->post('price'),
                'status' => NULL !== $this->input->post('status') ? $this->input->post('status') : NULL,
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not add location to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function index_post($id = NULL){

        $output = array();

        if(isset($id)){

            try{
                $output = $this->{$this->controller}->read_one(array(
                    'loc_id' => $id
                ));
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve location from store: '.$e->getMessage());
            }
    
        }
        else{
            $filter = (NULL !== $this->input->post('filter')) ? $this->input->post('filter') : NULL;
            $page = (NULL !== $this->input->post('page')) ? $this->input->post('page') : 1;

            try{
                $output = $this->{$this->controller}->paginate((array) json_decode($filter), $page, 12, 'loc_id');
            }
            catch(Exception $e){
                return $this->fail('Could not retrieve locations from store: '.$e->getMessage());
            }

        }

        return $this->success($output);
    }

    public function update_post($id){
        $output = array();

        try{
            
            $prev_location = $this->{$this->controller}->read_one(array(
                'loc_id' => $id
            ));

            $input = array_filter($this->input->post(), function($v, $k){
                return in_array($k, $this->{$this->controller}->db_columns());
            }, ARRAY_FILTER_USE_BOTH);

            $output = $this->{$this->controller}->update(array(
                'loc_id' => $id
            ), $input);

        }
        catch(Exception $e){
            return $this->fail('Could not update location in store: '.$e->getMessage());
        }

        return $this->success($output);
    }

    public function delete_post($id){
        $output = array();

        try{
            $output = $this->{$this->controller}->soft_delete(array(
                'loc_id' => $id
            ));
        }
        catch(Exception $e){
            return $this->fail('Could not delete location to store: '.$e->getMessage());
        }

        return $this->success($output);
    }

}