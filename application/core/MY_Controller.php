<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class MY_Controller extends CI_Controller {

    protected $controller = NULL;
    protected $method = NULL;

    protected $per_page = 12;
    
    function __construct()
    {
        parent::__construct();
    }

}