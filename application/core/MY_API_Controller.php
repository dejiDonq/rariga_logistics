<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_API_Controller extends REST_Controller {

    protected $controller = NULL;
    protected $method = NULL;

    protected $per_page = 12;
    
    function __construct()
    {
        parent::__construct();

        $this->inputs();
        
        $this->controller = str_replace('-', '_', $this->uri->segment(2, NULL));
        $this->method = str_replace('-', '_', $this->uri->segment(3, NULL));

    }

    public function inputs()
    {
        $source = json_decode(file_get_contents("php://input"), true);
        $source = is_array($source) ?  $source : array(); 
        foreach ($source as $key => $value) {
            $_POST[$key] = $value;
        }
        return $source;
    }

    public function jsonify($status, $data = NULL, $message = NULL, $code = NULL){
        return $this->response(array(
            'status' => $status, //true|false
            'data' => $data,
            'message' => $message,
            'code' => $code,
        ));
    }

    public function html($html, $data = NULL, $status = 'success', $code = NULL){
        return $this->response(array(
            'status' => $status,
            'html' => $html,
            'data' => $data,
            'message' => NULL,
            'code' => $code,
        ));
    }

    // All went well, and (usually) some data was returned.
    public function success($data, $message = NULL){
        return $this->jsonify(
            true,
            $data,
            $message,
            200
        );
    }

    // There was a problem with the data submitted, or some pre-condition of the API call wasn't satisfied
    public function fail($message, $data = NULL){
        return $this->jsonify(
            false,
            $data,
            $message,
            200
        );
    }

}