<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class MY_Model extends CI_Model {

    protected $table = "";

    protected $primary_key = "";
    
    protected $title_key = "";

    protected $columns = array();

    protected $fillable = array();

    protected $hidden = array();
    
    protected $parent = array();

    protected $parents = array();
    
    protected $child = array();

    protected $children = array();

    function __construct()
    {
        parent::__construct();
    }

    public function pre_create($pre_data)
    {
        return $pre_data;
    }

    public function post_create($post_data)
    {
        return $post_data;
    }

    public function table_name()
    {
        return $this->table;
    }

    public function columns()
    {
        return $this->columns;
    }

    public function fillable()
    {
        return array_keys($this->columns_map());
    }

    public function hidden()
    {
        return $this->hidden;
    }

    public function show()
    {
        return array_diff($this->fillable(), $this->hidden());
    }    

	public function to_select()
	{
		return $this->show();
    }
    
    public function primary_key()
    {
        return $this->primary_key;
    }
    
    public function title_key()
    {
        return $this->primary_key;
    }
    
    public function parent(){
        return $this->parent;
    }

    public function child(){
        return $this->child;
    }

    public function parents(){
        return $this->parents;
    }

    public function children(){
        return $this->children;
    }

    public function table($table, $primary_key = NULL)
    {
        $this->table = $table;
        $this->primary_key = isset($primary_key) ? $primary_key : $this->inflector->singularize($table).'_id';
        return clone $this;
    }

    public function select($fields)
    {

        $this->fields = $fields;
        return clone $this;
    }

    public function uuid(){
        return Uuid::uuid4()->toString();
    }

    public function create($data, $is_batch = FALSE){

        if($is_batch){

            return $this->db->insert_batch($this->table, $data); 
        }
        
        $this->db->insert($this->table, array_merge(
            $data,
            array(
                'updated' => date("Y-m-d H:i:s"),
                'created' => date("Y-m-d H:i:s"),
            )
        ));

        $after = $this->read_one(array($this->primary_key =>  $this->db->insert_id()));

        return $after;
    }

    public function update($where, $data, $is_batch = FALSE){

        if($is_batch){

            return $this->db->where($where)->update_batch($this->table, $data); 
        }

        $before = $this->read_one($where);

        $this->db->where($where)->update($this->table, array_merge(
            $data,
            array(
                'updated' => date("Y-m-d H:i:s"),
            )
        ));
        
        $after = $this->read_one($where);

        return $after;
    }

    public function upsert($where, $data, $is_batch = FALSE){
        
        if($this->count($where) > 0){
            $this->update($where, $data, $is_batch);
        }
        else{
            $this->create($data, $is_batch);
        }

        return $this->read_one($where);
    }

    public function read_one($where){
        
        if(!empty($this->fields)){
            $this->db->select($this->fields);
        }
        
        $after = $this->db->where(array_merge(
            $where,
            array(
                // 'status !=' => 'deleted'
            )
        ))->get($this->table)->row_array();

        return $after;

    }
    
    public function read_many($where, $offset = 0, $limit = 12, $orderBy = 'updated', $direction = 'DESC'){
        return $this->db->where(array_merge(
            $where,
            array(
                'status !=' => 'deleted'
            )
        ))
        // ->order_by($orderBy, $direction)->get($this->table)->result_array();
        ->order_by($orderBy, $direction)->limit($limit, $offset)->get($this->table)->result_array();
    } 
    
    public function read_many_in($column, $in, $where = array(), $limit = 12, $offset = 0, $orderBy = 'updated', $direction = 'DESC'){
        
        return $this->db->where_in($column, $in)->where(array_merge(
            $where,
            array(
                'status !=' => 'deleted'
            )
        ))->order_by($orderBy, $direction)->get($this->table)->result_array();
    }
    
    public function read_all($where = array()){
        return $this->db->where(array_merge(
            $where,
            array(
                'status !=' => 'deleted'
            )
        ))->get($this->table)->result_array();
    }

    public function paginate($where, $page = 1, $per_page = 12, $order_by = NULL, $appends_url = 'collection'){
        
        $total = $this->count($where);
        $limit  = $per_page;
        $offset  = ($per_page) * ($page - 1);

        $items = $this->read_many($where, $offset, $limit, $order_by);
        return array(
            'items' => $items,
            'sub_total' => count($items),
            'total' => $total,
        );
    }

    public function soft_delete($where){
        
        $before = $this->read_one($where);

        $this->db->where($where)->update($this->table, array_merge(
            array(
                'status' => 'deleted'
            )
        ));

        return $before;
    }

    public function delete($where){
        $deleted = $this->read_one($where);
        $this->db->where($where)->delete($this->table);
        return $deleted;
    }

    public function exist($where){
        return $this->count(array_merge(
            $where,
            array(
                'status !=' => 'deleted'
            )
        )) > 0;
    }

    public function count($where){
        return $this->db->where(array_merge(
            $where,
            array(
                'status !=' => 'deleted'
            )
        ))->from($this->table)->count_all_results();
    }

    public function count_all(){
        return  $this->db->count_all($this->table);
    }

    /**
    * The following data is available for if $only_names is FALSE:
    * name - column name
    * max_length - maximum length of the column
    * primary_key - 1 if the column is a fieldsy key
    * type - the type of the column
    */
    public function db_columns($only_names = TRUE)
    {
        return ($only_names) ? $this->db->list_fields($this->table) : $this->db->field_data($this->table);
    }

    public function at_columns()
    {
        return array_filter($this->columns(), function ($column){
            return ends_with($column, '_time');
        });
    }

    public function id_columns()
    {
        return array_filter($this->columns(), function ($column){
            return ends_with($column, '_id');
        });
    }

    public function type_columns()
    {
        return array_filter($this->columns(), function ($column){
            return ends_with($column, 'type');
        });
    }

    public function src_columns()
    {
        return array_filter($this->columns(), function ($column){
            return ends_with($column, '_src');
        });
    }    

	public function relative_columns()
	{
		return array();
    }
    
    public function paginate_with($where, $page = 1, $per_page = 12, $appends_url = 'collection'){
        
        $total = $this->count($where);
        $limit  = $per_page;
        $offset  = ($per_page) * ($page - 1);

        $this->pagination->initialize(array(
            'total_rows' => $total,
            'per_page' => $limit ,
            'base_url' => site_url().'/'.$this->table.'/'.$appends_url,
        ));
        
        return array(
            'items' => $this->read_many_with($where, $limit, $offset),
            'total' => $total,
            'links' => $this->pagination->create_links(),
        );
    }

    public function view_query($method, $parameters){
        return $this->{$method}(...$parameters);
    }
    
    public function special_count($table, $where = array()){
        return $this->db->where(array_merge(
            $where,
            array(
                'status !=' => 'deleted'
            )
        ))->from($this->table)->count_all_results();
    }

}