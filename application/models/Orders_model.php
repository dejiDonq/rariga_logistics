<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends MY_Model
{
	protected $table = "orders";
	
    public $primary_key = "ord_id";
    
	public $title_key = "descr";

    public $columns = array();
	
    public $hidden = array();

	protected $parent = array();
	
    protected $parents = array();
    
	protected $child = array();
	
	protected $children = array();
	
	public function __construct()
	{
		parent::__construct();
	}
	
    public function pre_create($pre_data)
    {
        return array_merge(
			$pre_data
		);
	}
	
    public function post_create($post_data)
    {
        return array_merge(
			$post_data
		);
	}

}