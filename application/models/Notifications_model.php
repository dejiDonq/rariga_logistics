<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_model extends MY_Model
{
	protected $table = "notifications";
	
    public $primary_key = "id";
    
	public $title_key = "message";

    public $columns = array();
	
    public $hidden = array();

	protected $parent = array();
	
    protected $parents = array();
    
	protected $child = array();
	
	protected $children = array();
	
	public function __construct()
	{
		parent::__construct();
	}
	
    public function pre_create($pre_data)
    {
        return array_merge(
			$pre_data
		);
	}
	
    public function post_create($post_data)
    {
        return array_merge(
			$post_data
		);
	}

}